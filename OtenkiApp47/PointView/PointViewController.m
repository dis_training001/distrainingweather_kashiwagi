//
//  PointViewController.m
//  OtenkiApp47
//
//  Created by Beehive on 2018/06/08.
//  Copyright © 2018年 co.di-system. All rights reserved.
//

#import "PointViewController.h"
#import "SVProgressHUD.h"
#import <CoreLocation/CoreLocation.h>

@interface PointViewController () <NSURLSessionDelegate,CLLocationManagerDelegate>
@property (weak, nonatomic) IBOutlet UILabel *wetherLabel;
@property (nonatomic,retain) CLLocationManager *locationManager;
@property (weak, nonatomic) NSString *urlString;
@property (strong, nonatomic) NSMutableData *responsData;

@end


@implementation PointViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    if (nil == self.locationManager) {
        self.locationManager = [[CLLocationManager alloc] init];
        if ([[[UIDevice currentDevice] systemVersion] floatValue] >= 8.0) {
            [self.locationManager requestWhenInUseAuthorization];
        }
    }
    self.locationManager.delegate = self;
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


- (IBAction)pushedRequestButton:(id)sender
{
    [SVProgressHUD show];
    [self.locationManager requestLocation];
}



- (void) locationManager:(CLLocationManager *)manager
      didUpdateLocations:(NSArray<CLLocation *> *)locations
{
    CLLocation *newLocation = [locations lastObject];
    
    double latitude = newLocation.coordinate.latitude;
    double longitude = newLocation.coordinate.longitude;
    
    self.urlString = [NSString stringWithFormat:@"http://api.openweathermap.org/data/2.5/weather?lat=%f&lon=%f&appid=216d8cade8f1f3ea1eb8d03d017f660d",latitude,longitude];
    NSURL *url = [NSURL URLWithString:self.urlString];
    NSURLSessionConfiguration *config = [NSURLSessionConfiguration defaultSessionConfiguration];
    NSURLSession *session = [NSURLSession sessionWithConfiguration:config delegate:self delegateQueue:nil];
    NSURLSessionDataTask *task = [session dataTaskWithURL:url];
    [task resume];
}


- (void) locationManager:(CLLocationManager *)manager
        didFailWithError:(nonnull NSError *)error
{
    dispatch_async(dispatch_get_main_queue(), ^{
        self.wetherLabel.text = @"ERROR";
        [SVProgressHUD dismiss];
    });
}


- (void) URLSession:(NSURLSession *)session
           dataTask:(NSURLSessionDataTask *)dataTask
 didReceiveResponse:(NSURLResponse *)response
  completionHandler:(void (^)(NSURLSessionResponseDisposition))completionHandler
{
    self.responsData = [NSMutableData data];
    completionHandler(NSURLSessionResponseAllow);
}


- (void) URLSession:(NSURLSession *)session
           dataTask:(NSURLSessionDataTask *)dataTask
     didReceiveData:(NSData *)data
{
    [self.responsData appendData:data];
}


- (void) URLSession:(NSURLSession *)session
               task:(NSURLSessionTask *)task
didCompleteWithError:(NSError *)error
{
    NSDictionary *json = [NSJSONSerialization JSONObjectWithData:self.responsData options:NSJSONReadingAllowFragments error:nil];
    
    dispatch_async(dispatch_get_main_queue(), ^{
        self.wetherLabel.text = [json[@"weather"] firstObject][@"main"];
        [SVProgressHUD dismiss];
    });
}


@end
