//
//  DataViewController.m
//  OtenkiApp47
//
//  Created by Beehive on 2018/06/06.
//  Copyright © 2018年 co.di-system. All rights reserved.
//

#import "DataViewController.h"

typedef NS_ENUM(NSInteger,weatherDataEnum)
{
    todouhukenEnum = 0,
    weatherImageEnum = 1,
    weatherLabelEnum = 2,
    temperatureLabelEnum = 3,
    hiraganaEnum = 4,
};

@interface DataViewController ()
@property (weak, nonatomic) IBOutlet UILabel *todouhukenLabel;
@property (weak, nonatomic) IBOutlet UIImageView *weatherImage;
@property (weak, nonatomic) IBOutlet UILabel *weatherLabel;
@property (weak, nonatomic) IBOutlet UILabel *temperatureLabel;
@end

@implementation DataViewController


- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    self.todouhukenLabel.text = self.pointData[todouhukenEnum];
    self.weatherLabel.text = self.pointData[weatherLabelEnum];
    [self.weatherImage setImage:self.weatherIcon];
    // 気温の計算
    NSString *string = [NSString stringWithFormat:@"%@",self.pointData[temperatureLabelEnum]];
    NSInteger integer = [string intValue];
    integer = integer - 273;
    string = [NSString stringWithFormat:@"%ld",integer];
    self.temperatureLabel.text = string;
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


- (void) viewDidDisappear:(BOOL)animated
{
    [self dismissViewControllerAnimated:YES completion:nil];
}


@end
