//
//  DataViewController.h
//  OtenkiApp47
//
//  Created by Beehive on 2018/06/06.
//  Copyright © 2018年 co.di-system. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface DataViewController : UIViewController

@property (strong ,nonatomic) NSMutableArray *pointData;
@property (strong, nonatomic) UIImage *weatherIcon;

@end
