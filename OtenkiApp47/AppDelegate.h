//
//  AppDelegate.h
//  OtenkiApp47
//
//  Created by Beehive on 2018/06/04.
//  Copyright © 2018年 co.di-system. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

