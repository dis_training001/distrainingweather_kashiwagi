//
//  ViewController.m
//  OtenkiApp47
//
//  Created by Beehive on 2018/06/04.
//  Copyright © 2018年 co.di-system. All rights reserved.
//

#import "ViewController.h"
#import "CustomCell.h"
#import "SessionManager.h"
#import "DataViewController.h"
#import "SVProgressHUD.h"

typedef NS_ENUM(NSInteger,weatherDataEnum)
{
    todouhukenEnum = 0,
    weatherImageEnum = 1,
    weatherLabelEnum = 2,
    temperatureLabelEnum = 3,
    hiraganaEnum = 4,
};

@interface ViewController () <UITableViewDelegate,UITableViewDataSource,UISearchBarDelegate,SessionManagerDelegate>
@property (weak, nonatomic) IBOutlet UISearchBar *serchBar;
@property (weak, nonatomic) IBOutlet UITableView *todouhukenList;
@property (weak, nonatomic) NSString *cellName;
@property (strong, atomic) NSMutableArray *allTodouhukenData;
@property (strong, atomic) NSMutableArray *showTodouhukenData;
@property (strong, nonatomic) CustomCell *cell;
@property (strong, nonatomic) SessionManager *sessionManager;
@property (strong, nonatomic) NSArray *urlArray;
@property (strong, nonatomic) NSArray *searchArray;
@property int cuntURL;
@property NSInteger cellNum;
@property (strong, nonatomic) NSUserDefaults *userDefaults;
@end
@implementation ViewController


- (void)viewDidLoad {
    [super viewDidLoad];
    self.cellNum = 47;
    self.serchBar.delegate = self;
    self.sessionManager = [[SessionManager instanceObject] init];
    self.sessionManager.delegate = self;
    self.allTodouhukenData = [NSMutableArray array];
    self.userDefaults = [NSUserDefaults standardUserDefaults];
    
    self.urlArray = @[
                          @"http://api.openweathermap.org/data/2.5/group?id=2130037,2130656,2113124,2112518,2110554,2111888,2112922,2112669,1850310,1863501,1853226,1850147,2113014,1860291,1855429,1849872,1861387,1863983,1856210,1863640&appid=216d8cade8f1f3ea1eb8d03d017f660d",
                          @"http://api.openweathermap.org/data/2.5/group?id=1848649,1851715,1865694,1852553,1857352,1857910,1855608,1853908,1848938,1862047,1849890,1854381,1852442,1862413,1848681,1860834,1864226,1850157,1859133,1863958&appid=216d8cade8f1f3ea1eb8d03d017f660d",
                          @"http://api.openweathermap.org/data/2.5/group?id=1853299,1856156,1854484,1858419,1856710,1860825,1854345&appid=216d8cade8f1f3ea1eb8d03d017f660d"
                          ];
    
    [SVProgressHUD show];
    
    self.cuntURL = 0;
    NSURL *url = [NSURL URLWithString:self.urlArray[0]];
    NSURLSessionDataTask *task = [self.sessionManager.session dataTaskWithURL:url];
    [task resume];
//    [SVProgressHUD dismiss];
    [self prepareTableView];
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


- (void) viewWillAppear:(BOOL)animated
{
    [SVProgressHUD show];
    [self.todouhukenList reloadData];
    [SVProgressHUD dismiss];
    
}

- (void) prepareTableView
{
    self.todouhukenList.delegate = self;
    self.todouhukenList.dataSource = self;
    
    self.cellName = NSStringFromClass([CustomCell class]);
    [self.todouhukenList registerNib:[UINib nibWithNibName:self.cellName bundle:nil] forCellReuseIdentifier:self.cellName];
}


- (NSInteger) tableView:(UITableView *)tableView
  numberOfRowsInSection:(NSInteger)section
{
    return self.cellNum;
}


- (CGFloat) tableView:(UITableView *)tableView
heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 100;
}


- (UITableViewCell *) tableView:(UITableView *)tableView
          cellForRowAtIndexPath:(nonnull NSIndexPath *)indexPath
{
    self.cell = [tableView dequeueReusableCellWithIdentifier:self.cellName forIndexPath:indexPath];
    [self.cell.starButton addTarget:self action:@selector(buttonPush:event:) forControlEvents:UIControlEventTouchUpInside];
    
    NSString *todouhukenString = self.showTodouhukenData[indexPath.row][todouhukenEnum];
    self.cell.todouhukenLabel.text = self.showTodouhukenData[indexPath.row][todouhukenEnum];
    self.cell.weatherLabel.text = self.showTodouhukenData[indexPath.row][weatherLabelEnum];
    
    NSString *imageID = self.showTodouhukenData[indexPath.row][weatherImageEnum];
    UIImage *image = [UIImage imageNamed:imageID];
    [self.cell.weatherImage setImage:image];
    
    if (todouhukenString != nil) {
        imageID = [self.userDefaults stringForKey:todouhukenString];
    }
    
    image = [UIImage imageNamed:imageID];
    [self.cell.starButton setImage:image forState:UIControlStateNormal];
    
    return self.cell;
}


- (void)tableView:(UITableView *)tableView
didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    DataViewController *dataViewController = [[DataViewController alloc] init];
    [self.navigationController pushViewController:dataViewController animated:YES];
    
    NSString *weatherID = self.showTodouhukenData[indexPath.row][weatherImageEnum];
    UIImage *weatherImage = [UIImage imageNamed:weatherID];
    
    dataViewController.pointData = self.showTodouhukenData[indexPath.row];
    dataViewController.weatherIcon = weatherImage;
}


- (void) weatherDataPass:(NSMutableArray*)data
{
    dispatch_async(dispatch_get_main_queue(), ^{
        self.cuntURL++;
        if(self.cuntURL == 1)
        {
            NSURL *url = [NSURL URLWithString:self.urlArray[1]];
            NSURLSessionDataTask *task = [self.sessionManager.session dataTaskWithURL:url];
            [task resume];
        }
        else if (self.cuntURL == 2)
        {
            NSURL *url = [NSURL URLWithString:self.urlArray[2]];
            NSURLSessionDataTask *task = [self.sessionManager.session dataTaskWithURL:url];
            [task resume];
        }
        else if (self.cuntURL >= 3)
        {
            self.showTodouhukenData = [NSMutableArray array];
            for (int i=0; i<data.count; i++) {
                [self.showTodouhukenData addObject:data[i]];
            }
            self.allTodouhukenData = self.showTodouhukenData;
            [self.todouhukenList reloadData];
            
            
            [self.userDefaults setObject:self.allTodouhukenData forKey:@"allArray"];
       
            [SVProgressHUD dismiss];
        }
    });
}


- (void) searchBar:(UISearchBar *)searchBar
     textDidChange:(NSString *)searchText
{
    NSString *searchBarText = searchBar.text;
    NSUInteger num = [searchBarText length];
    if (!num) {
        self.showTodouhukenData = self.allTodouhukenData;
        
        dispatch_async(dispatch_get_main_queue(), ^{
            self.cellNum = self.showTodouhukenData.count;
            [self.todouhukenList reloadData];
        });
    }
}


- (void) searchBarCancelButtonClicked:(UISearchBar *)searchBar
{
    [self.serchBar resignFirstResponder];
}


- (void) searchBarSearchButtonClicked:(UISearchBar *)searchBar
{
    NSString *searchBarText = searchBar.text;
    
    dispatch_async(dispatch_get_main_queue(), ^{
        NSString *searchString = [NSString stringWithFormat:@"SELF[FIRST] CONTAINS '%@'",searchBarText];
        NSPredicate *predicate = [NSPredicate predicateWithFormat:searchString];
        NSArray *result = [self.allTodouhukenData filteredArrayUsingPredicate:predicate];
        
        if(!result.count){
            searchString = [NSString stringWithFormat:@"SELF[LAST] CONTAINS '%@'",searchBarText];
            predicate = [NSPredicate predicateWithFormat:searchString];
            result = [self.allTodouhukenData filteredArrayUsingPredicate:predicate];
        }
        
        self.showTodouhukenData = [NSMutableArray array];
        for (NSArray *array in result) {
            [self.showTodouhukenData addObject:array];
        }
        self.cellNum = self.showTodouhukenData.count;
        [self.todouhukenList reloadData];
    });
    
    [self.serchBar resignFirstResponder];
}


- (void)buttonPush:(id)sender
             event:(UIEvent *)event
{
    [SVProgressHUD show];
    UITouch *touch = [[event allTouches] anyObject];
    CGPoint point = [touch locationInView:self.todouhukenList];
    NSIndexPath *indexPath = [self.todouhukenList indexPathForRowAtPoint:point];
    CustomCell *cell = [self.todouhukenList cellForRowAtIndexPath:indexPath];
    NSString *starImage = [self.userDefaults stringForKey:cell.todouhukenLabel.text];

    if ([starImage isEqualToString:@"starOFF"]) {
        [self.userDefaults setObject:@"starON" forKey:self.showTodouhukenData[indexPath.row][todouhukenEnum]];
        [self.userDefaults synchronize];
    }
    else{
        [self.userDefaults setObject:@"starOFF" forKey:self.showTodouhukenData[indexPath.row][todouhukenEnum]];
        [self.userDefaults synchronize];
    }
    
    [self.todouhukenList reloadData];
    [SVProgressHUD dismiss];
    
}


@end
