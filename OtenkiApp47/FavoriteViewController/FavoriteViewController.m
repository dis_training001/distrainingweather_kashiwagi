//
//  FavoriteViewController.m
//  OtenkiApp47
//
//  Created by Beehive on 2018/06/11.
//  Copyright © 2018年 co.di-system. All rights reserved.
//

#import "FavoriteViewController.h"
#import "CustomCell.h"
#import "DataViewController.h"
#import "SVProgressHUD.h"
#import "SessionManager.h"

typedef NS_ENUM(NSInteger,weatherDataEnum)
{
    todouhukenEnum = 0,
    weatherImageEnum = 1,
    weatherLabelEnum = 2,
    temperatureLabelEnum = 3,
    hiraganaEnum = 4,
};

@interface FavoriteViewController () <UITableViewDelegate,UITableViewDataSource,UISearchBarDelegate>
@property (weak, nonatomic) IBOutlet UITableView *favoriteList;
@property (weak, nonatomic) IBOutlet UISearchBar *searchBar;
@property (strong, nonatomic) NSUserDefaults *userDefaults;
@property (strong, nonatomic) CustomCell *cell;
@property (strong, atomic) NSArray *allArray;
@property (strong, atomic) NSMutableArray *favoriteArray;
@property (strong, atomic) NSMutableArray *showArray;
@property (weak, nonatomic) NSString *cellName;
@property NSInteger cellNum;
@end

@implementation FavoriteViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    self.searchBar.delegate = self;
    self.showArray = [NSMutableArray array];
    self.favoriteArray = [NSMutableArray array];
    self.userDefaults = [NSUserDefaults standardUserDefaults];
    self.allArray = [self.userDefaults arrayForKey:@"allArray"];
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


- (void) viewDidAppear:(BOOL)animated
{
    [SVProgressHUD show];
    self.showArray = [NSMutableArray array];
    for (NSArray *pointData in self.allArray) {
        if ([@"starON" isEqualToString:[self.userDefaults stringForKey:pointData[todouhukenEnum]]]) {
            [self.showArray addObject:pointData];
        }
    }
    self.favoriteArray = self.showArray;
    [self prepareTableView];
    [SVProgressHUD dismiss];
}

- (void) prepareTableView
{
    self.favoriteList.delegate = self;
    self.favoriteList.dataSource = self;
    
    self.cellName = NSStringFromClass([CustomCell class]);
    [self.favoriteList registerNib:[UINib nibWithNibName:self.cellName bundle:nil] forCellReuseIdentifier:self.cellName];
    [self.favoriteList reloadData];
}


- (NSInteger) tableView:(UITableView *)tableView
  numberOfRowsInSection:(NSInteger)section
{
    return self.showArray.count;
}


- (CGFloat) tableView:(UITableView *)tableView
heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 100;
}


- (UITableViewCell *) tableView:(UITableView *)tableView
          cellForRowAtIndexPath:(nonnull NSIndexPath *)indexPath
{
    self.cell = [tableView dequeueReusableCellWithIdentifier:self.cellName forIndexPath:indexPath];
    [self.cell.starButton addTarget:self action:@selector(buttonPush:event:) forControlEvents:UIControlEventTouchUpInside];
    
    NSString *todouhukenString = self.showArray[indexPath.row][todouhukenEnum];
    self.cell.todouhukenLabel.text = self.showArray[indexPath.row][todouhukenEnum];
    self.cell.weatherLabel.text = self.showArray[indexPath.row][weatherLabelEnum];

    NSString *imageID = self.showArray[indexPath.row][weatherImageEnum];
    UIImage *image = [UIImage imageNamed:imageID];
    [self.cell.weatherImage setImage:image];

    if (todouhukenString != nil) {
        imageID = [self.userDefaults stringForKey:todouhukenString];
    }

    image = [UIImage imageNamed:imageID];
    [self.cell.starButton setImage:image forState:UIControlStateNormal];

    return self.cell;
}


- (void)tableView:(UITableView *)tableView
didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    DataViewController *dataViewController = [[DataViewController alloc] init];
    [self.navigationController pushViewController:dataViewController animated:YES];
    
    NSString *weatherID = self.showArray[indexPath.row][weatherImageEnum];
    UIImage *weatherImage = [UIImage imageNamed:weatherID];
    
    dataViewController.pointData = self.showArray[indexPath.row];
    dataViewController.weatherIcon = weatherImage;
}


- (void)buttonPush:(id)sender
             event:(UIEvent *)event
{
    [SVProgressHUD show];
    UITouch *touch = [[event allTouches] anyObject];
    CGPoint point = [touch locationInView:self.favoriteList];
    NSIndexPath *indexPath = [self.favoriteList indexPathForRowAtPoint:point];
    
    [self.userDefaults setObject:@"starOFF" forKey:self.showArray[indexPath.row][todouhukenEnum]];
    [self.userDefaults synchronize];
    [self.showArray removeObjectAtIndex:indexPath.row];
    
    self.favoriteArray = self.showArray;
    
    [self.favoriteList reloadData];
    [SVProgressHUD dismiss];
}


- (void) searchBar:(UISearchBar *)searchBar
     textDidChange:(NSString *)searchText
{
    NSString *searchBarText = searchBar.text;
    NSUInteger num = [searchBarText length];
    if (!num) {
        self.showArray = self.favoriteArray;
        
        dispatch_async(dispatch_get_main_queue(), ^{
            self.cellNum = self.showArray.count;
            [self.favoriteList reloadData];
        });
    }
}


- (void) searchBarCancelButtonClicked:(UISearchBar *)searchBar
{
    [self.searchBar resignFirstResponder];
}


- (void) searchBarSearchButtonClicked:(UISearchBar *)searchBar
{
    NSString *searchBarText = searchBar.text;
    
    dispatch_async(dispatch_get_main_queue(), ^{
        NSString *searchString = [NSString stringWithFormat:@"SELF[FIRST] CONTAINS '%@'",searchBarText];
        NSPredicate *predicate = [NSPredicate predicateWithFormat:searchString];
        NSArray *result = [self.favoriteArray filteredArrayUsingPredicate:predicate];
        
        if(!result.count){
            searchString = [NSString stringWithFormat:@"SELF[LAST] CONTAINS '%@'",searchBarText];
            predicate = [NSPredicate predicateWithFormat:searchString];
            result = [self.favoriteArray filteredArrayUsingPredicate:predicate];
        }
        
        self.showArray = [NSMutableArray array];
        for (NSArray *array in result) {
            [self.showArray addObject:array];
        }
        self.cellNum = self.showArray.count;
        [self.favoriteList reloadData];
    });
    
    [self.searchBar resignFirstResponder];
}

@end
