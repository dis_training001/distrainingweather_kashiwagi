//
//  SessionManager.h
//  OtenkiApp47
//
//  Created by Beehive on 2018/06/05.
//  Copyright © 2018年 co.di-system. All rights reserved.
//

#import <Foundation/Foundation.h>
@protocol SessionManagerDelegate <NSObject>
- (void) weatherDataPass:(NSMutableArray*)data;
@end

@interface SessionManager : NSObject
@property (strong, nonatomic) id <SessionManagerDelegate> delegate;
@property (strong, nonatomic) NSURLSession *session;
+ (SessionManager *)instanceObject;
@end
