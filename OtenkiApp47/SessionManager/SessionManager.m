//
//  SessionManager.m
//  OtenkiApp47
//
//  Created by Beehive on 2018/06/05.
//  Copyright © 2018年 co.di-system. All rights reserved.
//

#import "SessionManager.h"

@interface SessionManager () <NSURLSessionDelegate>
@property (strong, nonatomic) NSMutableData *responsData;
@property (strong, nonatomic) NSMutableArray *allTodouhukenData;
@property (strong, nonatomic) NSMutableArray *dataOUT;
@property (strong, nonatomic) NSMutableDictionary *todouhukenList;
@property (strong, nonatomic) NSMutableDictionary *todouhukenHiragana;
@property (strong, nonatomic) NSUserDefaults *userDefaults;
@end

@implementation SessionManager
static SessionManager *sharedData = nil;

+ (SessionManager *)instanceObject
{
    @synchronized(self)
    {
        if (!sharedData) {
            sharedData = [SessionManager new];
        }
        return sharedData;
    }
}


// イニシャライズ
- (id) init
{
    self = [super init];
    if (self) {
        NSURLSessionConfiguration *config = [NSURLSessionConfiguration defaultSessionConfiguration];
        self.session = [NSURLSession sessionWithConfiguration:config delegate:self delegateQueue:nil];
    }
    
    self.todouhukenList =
    [@{
       @"Hokkaidō":@"北海道",
       @"Aomori-ken":@"青森県",@"Akita-ken":@"秋田県",@"Iwate-ken":@"岩手県",@"Yamagata-ken":@"山形県",
       @"Miyagi-ken":@"宮城県",@"Fukushima-ken":@"福島県",
       @"Ibaraki-ken":@"茨城県",@"Tochigi-ken":@"栃木県",@"Gunma-ken":@"群馬県",@"Saitama-ken":@"埼玉県",
       @"Tokyo":@"東京都",@"Chiba-ken":@"千葉県",@"Kanagawa-ken":@"神奈川県",
       @"Niigata-ken":@"新潟県",@"Toyama-ken":@"富山県",@"Ishikawa-ken":@"石川県",@"Fukui-ken":@"福井県",
       @"Nagano-ken":@"長野県",@"Gifu-ken":@"岐阜県",@"Yamanashi-ken":@"山梨県",@"Shizuoka-ken":@"静岡県",
       @"Aichi-ken":@"愛知県",
       @"Shiga-ken":@"滋賀県",@"Mie-ken":@"三重県",@"Kyoto":@"京都府",@"Nara-ken":@"奈良県",
       @"Osaka":@"大阪府",@"Wakayama-ken":@"和歌山県",@"Hyōgo-ken":@"兵庫県",
       @"Tottori-ken":@"鳥取県",@"Okayama-ken":@"岡山県",@"Shimane-ken":@"島根県",@"Hiroshima-ken":@"広島県",
       @"Yamaguchi-ken":@"山口県",
       @"Kagawa-ken":@"香川県",@"Ehime-ken":@"愛媛県",@"Tokushima-ken":@"徳島県",@"Kōchi-ken":@"高知県",
       @"Fukuoka-ken":@"福岡県",@"Saga-ken":@"佐賀県",@"Nagasaki-ken":@"長崎県",@"Ōita-ken":@"大分県",
       @"Kumamoto-ken":@"熊本県",@"Miyazaki-ken":@"宮崎県",@"Kagoshima-ken":@"鹿児島県",
       @"Okinawa-ken":@"沖縄県"
       } mutableCopy];
    
    self.todouhukenHiragana =
    [@{
       @"Hokkaidō":@"ほっかいどう",
       @"Aomori-ken":@"あおもりけん",@"Akita-ken":@"あきたけん",@"Iwate-ken":@"いわてけん",@"Yamagata-ken":@"やまがたけん",
       @"Miyagi-ken":@"みやぎけん",@"Fukushima-ken":@"ふくしまけん",
       @"Ibaraki-ken":@"いばらきけん",@"Tochigi-ken":@"とちぎけん",@"Gunma-ken":@"ぐんまけん",@"Saitama-ken":@"さいたまけん",
       @"Tokyo":@"とうきょうと",@"Chiba-ken":@"ちばけん",@"Kanagawa-ken":@"かながわけん",
       @"Niigata-ken":@"にいがたけん",@"Toyama-ken":@"とやまけん",@"Ishikawa-ken":@"いしかわけん",@"Fukui-ken":@"ふくいけん",
       @"Nagano-ken":@"ながのけん",@"Gifu-ken":@"ぎふけん",@"Yamanashi-ken":@"やまなしけん",@"Shizuoka-ken":@"しずおかけん",
       @"Aichi-ken":@"あいちけん",
       @"Shiga-ken":@"しがけん",@"Mie-ken":@"みえけん",@"Kyoto":@"きょうとふ",@"Nara-ken":@"ならけん",
       @"Osaka":@"おおさかふ",@"Wakayama-ken":@"わかやまけん",@"Hyōgo-ken":@"ひょうごけん",
       @"Tottori-ken":@"とっとりけん",@"Okayama-ken":@"おかやまけん",@"Shimane-ken":@"しまねけん",
       @"Hiroshima-ken":@"ひろしまけん",@"Yamaguchi-ken":@"やまぐちけん",
       @"Kagawa-ken":@"かがわけん",@"Ehime-ken":@"えひめけん",@"Tokushima-ken":@"とくしまけん",@"Kōchi-ken":@"こうちけん",
       @"Fukuoka-ken":@"ふくおかけん",@"Saga-ken":@"さがけん",@"Nagasaki-ken":@"ながさきけん",@"Ōita-ken":@"おおいたけん",
       @"Kumamoto-ken":@"くまもとけん",@"Miyazaki-ken":@"みやざきけん",@"Kagoshima-ken":@"かごしまけん",
       @"Okinawa-ken":@"おきなわけん"
       } mutableCopy];
    self.dataOUT = [NSMutableArray array];
    self.userDefaults = [NSUserDefaults standardUserDefaults];
    return self;
}


- (void) URLSession:(NSURLSession *)session
          dataTask:(NSURLSessionDataTask *)dataTask
didReceiveResponse:(NSURLResponse *)response
 completionHandler:(void (^)(NSURLSessionResponseDisposition))completionHandler
{
    self.responsData = [NSMutableData data];
    completionHandler(NSURLSessionResponseAllow);
}


- (void) URLSession:(NSURLSession *)session
          dataTask:(NSURLSessionDataTask *)dataTask
    didReceiveData:(NSData *)data
{
    [self.responsData appendData:data];
}


- (void) URLSession:(NSURLSession *)session
               task:(NSURLSessionTask *)task
didCompleteWithError:(NSError *)error
{
    dispatch_async(dispatch_get_main_queue(), ^{
        NSDictionary *json = [NSJSONSerialization JSONObjectWithData:self.responsData options:NSJSONReadingAllowFragments error:nil];
        
        NSString *pointName;
        self.allTodouhukenData = [NSMutableArray array];
        self.allTodouhukenData = [json valueForKey:@"list"];
        
        int cunt=0;
        for (NSDictionary *pointTodouhuken in self.allTodouhukenData) {
            NSMutableArray *dataIN = [NSMutableArray array];
            
            pointName = self.todouhukenList[pointTodouhuken[@"name"]];
            [dataIN addObject:pointName];
            [dataIN addObject:[pointTodouhuken[@"weather"] firstObject][@"icon"]];
            [dataIN addObject:[pointTodouhuken[@"weather"] firstObject][@"main"]];
            [dataIN addObject:pointTodouhuken[@"main"][@"temp"]];
            
            if ([self.userDefaults stringForKey:pointName]) {
                [dataIN addObject:[self.userDefaults stringForKey:pointName]];
            }
            else{
                [self.userDefaults setObject:@"starOFF" forKey:pointName];
                [self.userDefaults synchronize];
            }
            
            pointName = self.todouhukenHiragana[pointTodouhuken[@"name"]];
            [dataIN addObject:pointName];
            
            [self.dataOUT addObject:dataIN];
            cunt++;
        }
        [self.delegate weatherDataPass:self.dataOUT];
    });
}


@end
