//
//  main.m
//  OtenkiApp47
//
//  Created by Beehive on 2018/06/04.
//  Copyright © 2018年 co.di-system. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
